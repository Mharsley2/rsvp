
const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = process.env.Port || 3000;
const url = "mongodb://localhost:27017/rsvp";
//app.use is middleware in this case its 
mongoose.connect(url);

let Rsvp = require('./models/rsvp')

app.use(express.urlencoded());
app.set('view engine', 'pug');

//mongoose.connect('mongodb://localhost/rsvp', {useNewUrlParser: true})
//let db = mongoose.connection 

// app.set('views', path.join(__dirname, "views"));


app.get('/', function(req, res){
    
    Rsvp.find({}, function(err, rsvps){
        if(err){
            console.log(err)
        }else {
        res.render('index', {
           actionPath: '/rsvps/add'
        })
    }
    })
})

 
app.post('/rsvps/add', function(req, res) {
   console.log(req.body)
   let newEntry = new Rsvp({
       name: req.body.name,
       email: req.body.email,
       attending: req.body.attending,
       numberAttending: req.body.number
   })
   newEntry.save()

   res.render('reply',{})
})

app.get('/guests', (req, res) => {
    Rsvp.find({}, (err, responses) => {
      console.log(responses)
      let attendArr = []
      let notAttendArr = []
      for (item of responses) {
        if (item.attending === 'yes') {
          attendArr.push(item)
        } else {
          notAttendArr.push(item)
        }
      }
      res.render('guests', { 
        attending: attendArr, 
        notAttending: notAttendArr 
      })
    })
    // console.log(currentDB)
    // res.send(currentDB)
})

app.listen(3000, function() {
  console.log('Server listening on port 3000...')
})
